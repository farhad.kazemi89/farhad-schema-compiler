const userCreateSchema = {
    type: "object",
    properties: {
        username: {
            description: "username",
            type: "string",
            minLength: 3,
            isNotEmpty: 1,
        },
        fname: {
            description: "first name of the user",
            type: "string",
            minLength: 3,
            isNotEmpty: 3,
        },
        lname: {
            description: "family name of the user",
            type: "string",
            minLength: 3,
            isNotEmpty: 1,
        },
        email: {
            description: "Name of the user",
            type: "string",
            //all the characters permitted by RFC 5322. restrict leading, trailing, or consecutive dots in emails. restrict no. of characters in top level domain.
            pattern: "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$",
        },
        password: {
            description: "Password of the user",
            type: "string",
            //Minimum six and maximum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.
            pattern: "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{6,8}$"
        },
        confirmPassword: {
            description: "Exact same password",
            type: "string",
            const: {
                $data: "1/password"
            }
        },
        role: {
            description: "Role can be only one of the three admin, support, user",
            type: "string",
            enum:
                [
                    "admin",
                    "support",
                    "user"
                ]
        },
    },
    required: ["username", "fname", "lname", "email", "password", "role"],
    additionalProperties: false
}

module.exports = userCreateSchema;


// const gooData = {
//     username: "JohnnyBoy",
//     fname: "John",
//     lname: "Cooper",
//     email: "user@domain.com",
//     password: "Answer$1",
//     confirmPassword: "Answer$1",
//     role: "admin",
// }

//fname can be like below:
//  fname: "u u"
