const Ajv2020 = require("ajv/dist/2020");
const ajv = new Ajv2020({
    allErrors: true,
    $data: true,
});

ajv.addKeyword({
    keyword: 'isNotEmpty',
    validate: (schema , data) => {
        // console.log(`-----schema:-----${schema}}`);
        if (schema){
            // console.log(`-----data:-----${data}}`);
            return typeof data === 'string' && data.trim().length >= schema
        }
        else return true;
    }
});

class SchemaCompiler {
    #eventEmitter;
    #schemas;
    #ajvCompile = {};

    constructor(config) {
        this.#eventEmitter = config.eventEmitter;
        this.#schemas = config.schemas;
        this.#setRequestListener();
        // console.log("-----schema compiler----created----listening on start")
    }

    #compile(schemas) {
        // console.log(`-----schema compiler requiring schemas----type of all schemas:${typeof schemas}`);
        // console.dir(schemas,{depth: null});
        for (const [key, value] of Object.entries(schemas)) {
            // console.log(`schema key is :${key}`, `schema value is: ${value}`);
            const schemaObject = require(value);
            this.#ajvCompile[key] = ajv.compile(schemaObject);
        }
        // console.log('all schemas are compiled into ajvCompile private object');
        // console.dir(this.#ajvCompile, {depth: null})
    }

    get ajvCompile() {
      return this.#ajvCompile;
    }

    #setRequestListener() {
        this.#eventEmitter.on('start', () => {
            // console.log("-----schema compiler-----heard start-----");
            this.#compile(this.#schemas);
        });
    }
}

module.exports = SchemaCompiler;